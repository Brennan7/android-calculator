package com.bazingatech.amazingcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Brennan on 10/30/2017.
 */

public class MainActivity extends AppCompatActivity  {

    // Fields
    private Button FbtnDelete, FbtnClear, FbtnParenthesis, FbtnEquals, FbtnReverseSign;
    private TextView FtvOutput,FtvCalculation;
    private String Fcalculation = "";
    private String Fresult = "0.0";
    private final int MAX_ELEMENTS = 25;
    private final String ERROR_OUTPUT = "Invalid format";
    private final String UNDEFINED_ERROR = "Undefined";
    private final char LEFT_PAREN = '(';
    private final char RIGHT_PAREN = ')';
    private final char CHAR_DIVISION = '/';
    private final char CHAR_MULTIPLICATION = '*';
    private final char CHAR_ADDITION = '+';
    private final char CHAR_SUBTRACTION = '-';
    private final char CHAR_DEC = '.';
    private final char CHAR_EXPON = '^';
    private final char CHAR_ZERO = '0';
    private boolean addedReverseSign = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Reference the views
        setUpTextViews();
        setUpButtonOnClicks();
    }

    /*
    * Function updates the calculator output
    * Parameters:
    * Returns: void
    */
    private void updateScreen(){
        FtvOutput.setText(Fresult);
        FtvCalculation.setText(Fcalculation);
    }

    /*
    * Function checks to see if a character is one of the operators
    * Parameters: char element
    * Returns: boolean
    */
    private void clear(){
        Fcalculation = "";
        Fresult = "0.0";
    }

    /*
    * Function checks to see if the Max Element count has been exceeded
    * Parameters:
    * Returns: boolean
    */
    private boolean hasMetCapElements() {
        if (Fcalculation.length() > MAX_ELEMENTS) {
            return true;
        } else {
            return false;
        }
    }

    /*
    * Function checks to see if a left parenthesis or right parenthesis should be used
    * Parameters:
    * Returns: boolean
    */
    private boolean isLeftParenthesis() {

        int leftParenCount = getLeftParenCount();   // Get left Parenthesis count
        int rightParenCount = getRightParenCount(); // Get Right Parenthesis count

        // If length is zero then return true
        if (Fcalculation.length() == 0) {
            return true;
        }

        // Check if previous character is a digit
        if (Character.isDigit(Fcalculation.charAt(Fcalculation.length() - 1))) {
            if (leftParenCount == rightParenCount) { // Check if paren counts are equal, if so then add multiplication operator to calculation and return true, otherwise return false
                Fcalculation += CHAR_MULTIPLICATION;
                return true;
            } else {
                return false;
            }
        }

        // Check if previous character is a right Paren
        if (Fcalculation.charAt(Fcalculation.length() - 1) == RIGHT_PAREN ) {
            if (leftParenCount == rightParenCount) { // Check if paren counts are equal, if so then add multiplication operator to calculation and return true, otherwise return false
                Fcalculation += CHAR_MULTIPLICATION;
                return true;
            } else {
                return false;
            }
        }

        // Check if previous character is left paren. If so, then return true
        if (Fcalculation.charAt(Fcalculation.length() - 1) == LEFT_PAREN ) {
            return true;
        }

        // Check if previous character is an operator. If so, then return true
        if (isElementAnOperator(Fcalculation.charAt(Fcalculation.length() - 1))) {
            return true;
        }

        // Check if previous character is a decimal. If so, add zero and a multiplication operator to calculation and return true
        if (Fcalculation.charAt(Fcalculation.length() - 1) == CHAR_DEC) {
            Fcalculation += CHAR_ZERO;
            Fcalculation += CHAR_MULTIPLICATION;
            return true;
        }

        return false; // If nothing checks out then return false
    }

    /*
    * Function checks to see if a character is one of the operators
    * Parameters: char element
    * Returns: boolean
    */
    private boolean isElementAnOperator(char element) {

        if (element == CHAR_ADDITION) {
            return true;
        } else if (element == CHAR_SUBTRACTION) {
            return true;
        } else if(element == CHAR_MULTIPLICATION) {
            return true;
        } else if(element == CHAR_DIVISION) {
            return true;
        } else if (element == CHAR_EXPON) {
            return true;
        } else {
            return false;
        }
    }

    /*
    * Function counts the amount of left parenthesis in calculation
    * Parameters:
    * Returns: int
    */
    private int getLeftParenCount() {
        int count = 0;
        for (int i=0; i < Fcalculation.length(); i++) {
            if (Fcalculation.charAt(i) == LEFT_PAREN) {
                count++;
            }
        }

        return count;
    }

    /*
    * Function counts the amount of right parenthesis in calculation
    * Parameters:
    * Returns: int
    */
    private int getRightParenCount() {
        int count = 0;
        for (int i=0; i < Fcalculation.length(); i++) {
            if (Fcalculation.charAt(i) == RIGHT_PAREN) {
                count++;
            }
        }

        return count;
    }

    /*
    * Function validates that the equation format is correct
    * Parameters:
    * Returns: boolean
    */
    private boolean isCorrectFormat() {

        int leftParenCount = getLeftParenCount();
        int rightParenCount = getRightParenCount();

        if (leftParenCount != rightParenCount) {
            setError();
            return false;
        } else if (Fcalculation.length() != 0) {
            if (doesUndefinedErrorExist()) {
                setUndefinedError();
                return false;
            } else if (isElementAnOperator(Fcalculation.charAt(Fcalculation.length() - 1))) {
                setError();
                return false;
            } else if (Fcalculation.charAt(Fcalculation.length() - 1) == CHAR_DEC) {
                Fcalculation +=  CHAR_ZERO;
                updateScreen();
                return true;
            } else {
                return true;
            }
        }  else {
            return true;
        }
    }

    /*
       * Function checks to see if there is a division which will have undefined result
       * Parameters:
       * Returns: boolean
       */
    private boolean doesUndefinedErrorExist() {

        boolean exists = false;

        for (int i = 0; i < Fcalculation.length(); i++) {

            if (Fcalculation.charAt(i) == CHAR_DIVISION) {

                if (i < Fcalculation.length() - 1) {

                    if (Fcalculation.charAt(i+1) == CHAR_ZERO) {

                        for (int j = i + 1; j < Fcalculation.length(); j++) {
                            if (Character.isDigit(Fcalculation.charAt(j)) && Fcalculation.charAt(j) != CHAR_ZERO) {
                                exists = false;
                                break;
                            } else if (isElementAnOperator(Fcalculation.charAt(j)) || Fcalculation.charAt(j) == LEFT_PAREN || Fcalculation.charAt(j) == RIGHT_PAREN || Fcalculation.length() - 1 == j) {
                                exists = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (exists) {
                break;
            }
        }

        return  exists;
    }

    /*
    * Function calculates the result of the String equation.
    * Parameters:
    * Returns: void
    */
    private void performCalculation() {

        if (isCorrectFormat()) { // If equation format is correct, then calculate result

            CalculateResult calculateResult = new CalculateResult(Fcalculation);

            Double result = calculateResult.getResult();
            Fresult = String.valueOf(result);
            updateScreen();
        }
    }

    // Inner Class CalculateResult used for parsing arithmetic expression
    public static class CalculateResult {

        // Fields
        private int pos = -1, element;
        private String calculation;
        private final char LEFT_PAREN = '(';
        private final char RIGHT_PAREN = ')';
        private final char CHAR_DIVISION = '/';
        private final char CHAR_MULTIPLICATION = '*';
        private final char CHAR_ADDITION = '+';
        private final char CHAR_SUBTRACTION = '-';
        private final char CHAR_DEC = '.';
        private final char CHAR_EXPON = '^';
        private final char CHAR_ZERO = '0';
        private final char CHAR_NINE = '9';

        // Constructor
        public CalculateResult(String calculation) {
            this.calculation = calculation;
        }

        public double getResult() {
            return parse();
        }

        private void nextChar() {

            if (++pos < calculation.length()) {
                element = calculation.charAt(pos);
            } else {
                element = -1;
            }
        }

        private boolean consumeChar(int charToConsume) {

            while (element == ' ') {
                nextChar();
            }

            if (element == charToConsume) {
                nextChar();
                return true;
            }

            return false;
        }

        private double parse() {

            nextChar();
            double result = parseExpression();

            if (pos < calculation.length()) {
                throw new RuntimeException("Unexpected: " + (char)element);
            }

            return result;
        }

        private double parseExpression() {

            double x = parseTerm();
            while (true) {
                if (consumeChar(CHAR_ADDITION)) {
                    x += parseTerm(); // addition
                } else if (consumeChar(CHAR_SUBTRACTION)) {
                    x -= parseTerm(); // subtraction
                } else {
                    return x;
                }
            }
        }

        private double parseTerm() {

            double x = parseFactor();
            while(true) {

                if(consumeChar(CHAR_MULTIPLICATION)) {
                    x *= parseFactor(); // Perform multiplication
                } else if (consumeChar(CHAR_DIVISION)) {
                    x /= parseFactor(); // Perform division
                } else {
                    return x;
                }
            }
        }

        private double parseFactor() {

            double x;
            int startPos = this.pos;

            if (consumeChar(CHAR_ADDITION)) {
                return parseFactor(); // Unary plus
            }

            if (consumeChar(CHAR_SUBTRACTION)) {
                return -parseFactor(); // Unary minus
            }

            if (consumeChar(LEFT_PAREN)) { // Handling Parenthesis

                x = parseExpression();
                consumeChar(RIGHT_PAREN);

            } else if ((element >= CHAR_ZERO && element <= CHAR_NINE) || element == CHAR_DEC) { // Handling Numbers

                while ((element >= CHAR_ZERO && element <= CHAR_NINE) || element == CHAR_DEC) {
                    nextChar();
                }

                x = Double.parseDouble(calculation.substring(startPos, this.pos));

            } else {
                throw new RuntimeException("Unexpected: " + (char) element);
            }

            if (consumeChar(CHAR_EXPON)) {
                x = Math.pow(x, parseFactor()); // Perform exponentiation
            }

            return x;
        }
    }

    /*
    * Function sets Error output to screen
    * Parameters:
    * Returns: void
    */
    private void setUndefinedError() {
        Fresult = UNDEFINED_ERROR;
        updateScreen();
    }

    private void setError() {
        Fresult = ERROR_OUTPUT;
        updateScreen();
    }

    /*
    * Function handles all button clicks besides Clear, Reverse Sign, Parenthesis, and Equals
    * Parameters: View v
    * Returns: void
    */
    public void onClick(View v) {

        if(!hasMetCapElements()) { // If exceeded max elements, then do nothing

            Button b = (Button) v;
            char newElement = b.getText().toString().charAt(0); // Get the new character from button text
            String buttonText = (b.getText().toString());

            if (isElementAnOperator(newElement) && addedReverseSign) {
                handleOperatorWithReverseSignTrue();
            }

            if (Fcalculation.length() == 0) {
                handleElementWithCalculationLengthZero(newElement, buttonText);

            } else if (Fcalculation.length() == 1 && Fcalculation.charAt(0) == CHAR_ZERO && Character.isDigit(newElement)) {

            } else {

                if (isElementAnOperator(Fcalculation.charAt(Fcalculation.length() - 1)) && isElementAnOperator(newElement)) {

                } else if (isElementAnOperator(newElement) && Fcalculation.charAt(Fcalculation.length() - 1) == LEFT_PAREN) {

                } else if (Fcalculation.charAt(Fcalculation.length() - 1) == CHAR_DEC) {
                    handlePreviousDecimalElement(newElement);
                } else if (newElement == CHAR_DEC) {
                   handleNewDecimal(newElement);
                } else {
                    addNewElement(newElement);
                }
            }
        }
    }

    private void handleOperatorWithReverseSignTrue() {

        if (Fcalculation.charAt(Fcalculation.length()-2) == LEFT_PAREN && Fcalculation.charAt(Fcalculation.length()-1)== CHAR_SUBTRACTION) {
            Fcalculation = Fcalculation.substring(0, Fcalculation.length() - 2);
            updateScreen();
            addedReverseSign = false;
        } else {

            if (Fcalculation.charAt(Fcalculation.length() - 1) == CHAR_DEC) {
                Fcalculation += CHAR_ZERO;
            }

            Fcalculation += RIGHT_PAREN;
            updateScreen();
            addedReverseSign = false;
        }
    }

    private void handleElementWithCalculationLengthZero(char newElement, String buttonText) {

        if ((newElement) == CHAR_DEC) {
            Fcalculation += CHAR_ZERO + buttonText;
            updateScreen();
        } else if (isElementAnOperator(newElement)) {

        } else {
            addNewElement(newElement);
        }
    }

    private void handlePreviousDecimalElement(char newElement) {

        if (newElement == CHAR_DEC) {

        } else if (!Character.isDigit(newElement)) {
            addZeroThenNewElement(newElement);
        } else {
            addNewElement(newElement);
        }
    }

    private void handleNewDecimal(char newElement) {

        if (isInvalidDecimal()) {

        } else {
            if (Fcalculation.charAt(Fcalculation.length() - 1) == RIGHT_PAREN) {
                Fcalculation += CHAR_MULTIPLICATION;
            }
            if (isElementAnOperator(Fcalculation.charAt(Fcalculation.length() - 1)) || Fcalculation.charAt(Fcalculation.length() - 1) == LEFT_PAREN
                    || Fcalculation.charAt(Fcalculation.length() - 1) == RIGHT_PAREN) {
                addZeroThenNewElement(newElement);
            } else {
                addNewElement(newElement);
            }
        }
    }

    /*
   * Function logic for when a reverse sign is clicked
   * Parameters:
   * Returns: void
   * TODO: This function needs extreme refactoring
   */
    private void handleReverseSign() {

        if (Fcalculation.length() == 0) {
            Fcalculation = LEFT_PAREN + String.valueOf(CHAR_SUBTRACTION);
            updateScreen();
            addedReverseSign = true;
        } else if (Fcalculation.equals("(-")) {
            Fcalculation = "";
            updateScreen();
            addedReverseSign = false;
        } else {
            if (isElementAnOperator(Fcalculation.charAt(Fcalculation.length() - 1)) || isPreviousElementLeftParen()) {
                if (Fcalculation.length() >= 2 && Fcalculation.charAt(Fcalculation.length() - 2) == LEFT_PAREN && Fcalculation.charAt(Fcalculation.length()-1) == CHAR_SUBTRACTION) {
                    Fcalculation = Fcalculation.substring(0, Fcalculation.length() - 2);
                    updateScreen();
                    addedReverseSign = false;
                } else {
                    Fcalculation += LEFT_PAREN + String.valueOf(CHAR_SUBTRACTION);
                    updateScreen();
                    addedReverseSign = true;
                }
            } else if (isPreviousElementRightParen()) {

            } else {

                for (int i = Fcalculation.length() - 1; i >= 0; i--) {

                    if (i == 0) {

                        Fcalculation = "(-" + Fcalculation;
                        updateScreen();
                        addedReverseSign = true;
                        break;
                    } else if (Character.isDigit(Fcalculation.charAt(i))) {

                        if (i==1 && Character.isDigit(Fcalculation.charAt(0))) {

                            Fcalculation = "(-" + Fcalculation;
                            updateScreen();
                            addedReverseSign = true;
                            break;
                        }  else if (i == 1 && Fcalculation.charAt(0) == LEFT_PAREN) {

                            Fcalculation = Fcalculation.substring(0, 1) + "(-" + Fcalculation.substring(1, Fcalculation.length());
                            updateScreen();
                            addedReverseSign = true;
                            break;
                        } else if (Fcalculation.substring(i-2, i).equals("(-")) {

                            Fcalculation = Fcalculation.substring(0, i-2) + Fcalculation.substring(i, Fcalculation.length());
                            updateScreen();
                            addedReverseSign = false;
                            break;
                        } else if (Fcalculation.charAt(i-1) == LEFT_PAREN) {

                            Fcalculation = Fcalculation.substring(0, i) + LEFT_PAREN + String.valueOf(CHAR_SUBTRACTION) + Fcalculation.substring(i, Fcalculation.length());
                            updateScreen();
                            addedReverseSign = true;
                            break;
                        }
                    } else if (Fcalculation.charAt(i) == LEFT_PAREN) {

                        Fcalculation = Fcalculation.substring(0, i + 1) + "(-" + Fcalculation.substring(i, Fcalculation.length());
                        updateScreen();
                        addedReverseSign = true;
                        break;
                    } else if (isElementAnOperator(Fcalculation.charAt(i))) {

                        if (Fcalculation.charAt(i-1) == LEFT_PAREN && Fcalculation.charAt(i) == CHAR_SUBTRACTION) {

                        } else {

                            Fcalculation = Fcalculation.substring(0, i + 1) + "(-" + Fcalculation.substring(i+1, Fcalculation.length());
                            updateScreen();
                            addedReverseSign = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    private boolean isPreviousElementLeftParen() {

        if (Fcalculation.charAt(Fcalculation.length() - 1) == LEFT_PAREN) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isPreviousElementRightParen() {

        if (Fcalculation.charAt(Fcalculation.length() - 1) == RIGHT_PAREN) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isInvalidDecimal() {

        boolean isInvalid = false;

        for (int i = Fcalculation.length() - 1; i >= 0; i--) {
            if (isElementAnOperator(Fcalculation.charAt(i)) || Fcalculation.charAt(i) == LEFT_PAREN || Fcalculation.charAt(i) == RIGHT_PAREN) {
                break;
            } else if (Fcalculation.charAt(i) == CHAR_DEC) {
                isInvalid = true;
                break;
            }
        }

        return isInvalid;
    }

    private void addNewElement(char newElement) {
        Fcalculation += (newElement);
        updateScreen();
    }

    private void addZeroThenNewElement(char newElement) {
        Fcalculation +=  CHAR_ZERO;
        Fcalculation += (newElement);
        updateScreen();
    }

    private void setUpButtonOnClicks() {

        FbtnEquals = (Button) findViewById(R.id.buttonEquals);
        FbtnEquals.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                performCalculation();
            }
        });

        FbtnDelete = (Button) findViewById(R.id.buttonDelete);
        FbtnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (Fcalculation.length() == 0) {

                } else if (Fcalculation.length() == 1) {
                    Fcalculation = "";
                } else if (Fcalculation.length() == 2) {
                    Fcalculation = Fcalculation.substring(0, 1);
                } else {
                    Fcalculation = Fcalculation.substring(0, Fcalculation.length() - 1);
                }

                updateScreen();
            }
        });

        FbtnClear = (Button) findViewById(R.id.buttonClear);
        FbtnClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clear();
                updateScreen();
            }
        });

        FbtnParenthesis = (Button) findViewById(R.id.buttonParenthesis);
        FbtnParenthesis.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!hasMetCapElements()) {
                    if(addedReverseSign) {

                        if (Fcalculation.length() >= 2) {

                            if (Fcalculation.charAt(Fcalculation.length() - 2) == LEFT_PAREN && Fcalculation.charAt(Fcalculation.length() - 1) == CHAR_SUBTRACTION) {

                                Fcalculation += LEFT_PAREN;
                                addedReverseSign = false;
                            } else if(isLeftParenthesis()) {
                                Fcalculation += LEFT_PAREN;
                                addedReverseSign = false;
                            } else {
                                Fcalculation += RIGHT_PAREN;
                                addedReverseSign = false;
                            }
                        } else {

                            Fcalculation += RIGHT_PAREN;
                            addedReverseSign = false;
                        }
                    } else {

                        if(isLeftParenthesis()) {
                            Fcalculation += LEFT_PAREN;
                        } else {
                            Fcalculation += RIGHT_PAREN;
                        }
                    }

                    updateScreen();
                }
            }
        });

        FbtnReverseSign = (Button) findViewById(R.id.buttonReverseSign);
        FbtnReverseSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                handleReverseSign();
            }
        });
    }

    private void setUpTextViews() {
        FtvOutput = (TextView) findViewById(R.id.textViewOutput);
        FtvCalculation = (TextView) findViewById(R.id.textViewCalculation);
    }
}
